import { WsReactBaseComponentInterface, WsReactBaseContainer } from '@thewhite/react-base-components';
import { isEqual } from 'lodash';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { FlexBox } from 'ws-react-flex-layout';
import { GlobalStore } from '../../../store/globalStore';
import WrapPage from '../../Global/components/wrapPage/wrapPage';
import MainTopMenu from '../../Main/components/mainTopMenu/mainTopMenuComponent';

interface MainWrapProps {
    children: any;
    globalStore?: GlobalStore;
    location: any;
}

export interface MainWrapInterface extends WsReactBaseComponentInterface {
    props: MainWrapProps;
}

@inject('globalStore')
@observer
export class MainWrap extends WsReactBaseContainer<MainWrapProps, {}> implements MainWrapInterface {

    constructor(props: MainWrapProps) {
        super(props);
        if (this.props.globalStore) {
            this.props.globalStore.handlerPressControl();
        }
    }

    componentDidMount() {
        if (this.props.globalStore) {
            this.props.globalStore.setLocation(this.props.location);
        }
        window.onload = () => console.log('1')
    }

    componentWillReceiveProps(newProps: MainWrapProps) {
        if (!isEqual(newProps.location, this.props.location) && !!this.props.globalStore) {
            this.props.globalStore.setLocation(newProps.location);
        }
    }

    render(): false | JSX.Element {
        return (
            <WrapPage
                globalStore={this.props.globalStore}
            >
                <FlexBox
                    id="main-wrap"
                    column="start stretch"
                    className="main-wrap"
                    ref="mainWrap"
                >
                    <FlexBox
                        grow="1"
                        shrink="0"
                    >
                        <MainTopMenu
                            location={this.props.location}
                        />
                        <div
                            className="main-body body-2-primary"
                        >
                            {this.props.children}
                        </div>
                    </FlexBox>
                </FlexBox>
            </WrapPage>
        );
    }
}

export default MainWrap;
