import * as t from 'io-ts';

export const MainType = t.interface({
    message: t.string,
    amount: t.number,
});

export interface MainDTO extends t.TypeOf<typeof MainType> {
}

class Main {
    message: string;
    amount: number;

    constructor(params: MainDTO) {
        this.message = params.message;
        this.amount = params.amount;
    }
}

export { Main };
