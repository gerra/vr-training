import * as t from 'io-ts';

export const TestCreateValueType = t.interface({
    id: t.string,
    name: t.union([t.string, t.null]),
    color: t.union([t.string, t.null]),
    shortName: t.union([t.string, t.null]),
});

export interface TestCreateValueDTO extends t.TypeOf<typeof TestCreateValueType> {
}

class TestCreateValue {
    id: string;
    name: string | null;
    color: string | null;
    shortName: string | null;

    constructor(params: TestCreateValueDTO) {
        this.id = params.id;
        this.name = params.name ?? null;
        this.color = params.color ?? null;
        this.shortName = params.shortName ?? null;
    }
}

export { TestCreateValue };
