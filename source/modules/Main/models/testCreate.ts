import * as t from 'io-ts';
import { TestCreateValue, TestCreateValueType } from './testCreateValue';

export const TestCreateType = t.interface({
    id: t.string,
    name: t.union([t.string, t.null]),
    values: t.array(TestCreateValueType),
    description: t.union([t.string, t.null]),
});

export interface TestCreateDTO extends t.TypeOf<typeof TestCreateType> {
}

class TestCreate {
    id: string;
    name: string | null;
    values: TestCreateValue[];
    description: string | null;

    constructor(params: TestCreateDTO) {
        this.id = params.id;
        this.name = params.name ?? null;
        this.description = params.description ?? null;
        this.values = !!params.values.length ? params.values.map(value => new TestCreateValue(value)) : [];
    }
}

export { TestCreate };
