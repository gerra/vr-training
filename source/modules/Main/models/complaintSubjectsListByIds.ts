import { SimpleObjectInterface } from '@thewhite/react-global-module/lib/interfaces';
import * as t from 'io-ts';
import ADDRESSABLE_TYPE from '../enums/addressableTypeEnum';
import BASE_STATUS from '../enums/baseStatusEnum';
import DECISION_DAYS_TYPE from '../enums/decisionDaysTypeEnum';
import NATURE_TYPE from '../enums/natureTypeEnum';
import { ComplaintSubjectsListByIdsCategory, ComplaintSubjectsListByIdsCategoryType } from './complaintSubjectsListByIdsCategory';
import { ComplaintSubjectsListByIdsRatingForOnlineReception, ComplaintSubjectsListByIdsRatingForOnlineReceptionType } from './complaintSubjectsListByIdsRatingForOnlineReception';

export const ComplaintSubjectsListByIdsType = t.interface({
    keyWords: t.array(t.string),
    id: t.union([t.string, t.null]),
    name: t.union([t.string, t.null]),
    color: t.union([t.string, t.null]),
    status: t.union([t.string, t.null]),
    shortName: t.union([t.string, t.null]),
    natureType: t.union([t.string, t.null]),
    orderNumber: t.union([t.number, t.null]),
    description: t.union([t.string, t.null]),
    addressableType: t.union([t.string, t.null]),
    decisionDaysType: t.union([t.string, t.null]),
    contactWithAuthor: t.union([t.boolean, t.null]),
    hideNewComplaints: t.union([t.boolean, t.null]),
    firstDecisionDays: t.union([t.number, t. null]),
    secondDecisionDays: t.union([t.number, t.null]),
    requiredEsiaAccount: t.union([t.boolean, t.null]),
    postponedDecisionDays: t.union([t.number, t.null]),
    onlineReceptionRegistry: t.union([t.boolean, t.null]),
    extensibleOfResponseDueDate: t.union([t.string, t.null]),
    category: t.union([ComplaintSubjectsListByIdsCategoryType, t.null]),
    ratingForOnlineReception: t.union([ComplaintSubjectsListByIdsRatingForOnlineReceptionType, t.null]),
});

export interface IComplaintSubjectListDTO extends t.TypeOf<typeof ComplaintSubjectsListByIdsType> {
}

class ComplaintSubjectsListByIds {
    id: string | null;
    keyWords: string[];
    name: string | null;
    color: string | null;
    shortName: string | null;
    orderNumber: number | null;
    description: string | null;
    firstDecisionDays: number | null;
    contactWithAuthor: boolean | null;
    secondDecisionDays: number | null;
    hideNewComplaints: boolean | null;
    requiredEsiaAccount: boolean | null;
    status: SimpleObjectInterface | null;
    postponedDecisionDays: number | null;
    onlineReceptionRegistry: boolean | null;
    natureType: SimpleObjectInterface | null;
    extensibleOfResponseDueDate: string | null;
    addressableType: SimpleObjectInterface | null;
    decisionDaysType: SimpleObjectInterface | null;
    category: ComplaintSubjectsListByIdsCategory | null;
    ratingForOnlineReception: ComplaintSubjectsListByIdsRatingForOnlineReception | null;

    constructor(params: IComplaintSubjectListDTO) {
        this.id = params.id ?? null;
        this.name = params.name ?? null;
        this.color = params.color ?? null;
        this.keyWords = params.keyWords ?? null;
        this.shortName = params.shortName ?? null;
        this.description = params.description ?? null;
        this.orderNumber = params.orderNumber ?? null;
        this.contactWithAuthor = params.contactWithAuthor ?? null;
        this.firstDecisionDays = params.firstDecisionDays ?? null;
        this.hideNewComplaints = params.hideNewComplaints ?? null;
        this.secondDecisionDays = params.secondDecisionDays ?? null;
        this.requiredEsiaAccount = params.requiredEsiaAccount ?? null;
        this.status = !!params.status ? BASE_STATUS[params.status] : null;
        this.postponedDecisionDays = params.postponedDecisionDays ?? null;
        this.onlineReceptionRegistry = params.onlineReceptionRegistry ?? null;
        this.extensibleOfResponseDueDate = params.extensibleOfResponseDueDate ?? null;
        this.natureType = !!params.natureType ? NATURE_TYPE[params.natureType] : null;
        this.addressableType = !!params.addressableType ? ADDRESSABLE_TYPE[params.addressableType] : null;
        this.category = !!params.category ? new ComplaintSubjectsListByIdsCategory(params.category) : null;
        this.decisionDaysType = !!params.decisionDaysType ? DECISION_DAYS_TYPE[params.decisionDaysType] : null;
        this.ratingForOnlineReception = !!params.ratingForOnlineReception ? new ComplaintSubjectsListByIdsRatingForOnlineReception(params.ratingForOnlineReception) : null;
    }
}

export { ComplaintSubjectsListByIds };
