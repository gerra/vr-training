import * as t from 'io-ts';

export const ComplaintSubjectsListByIdsRatingForOnlineReceptionType = t.interface({
    subjectId: t.union([t.string, t.null]),
    messageStationId: t.union([t.string, t.null]),
    lowerRatingForOnlineReception: t.union([t.number, t.null]),
});

export interface ComplaintSubjectsListByIdsRatingForOnlineReceptionDTO extends t.TypeOf<typeof ComplaintSubjectsListByIdsRatingForOnlineReceptionType> {
}

class ComplaintSubjectsListByIdsRatingForOnlineReception {
    subjectId: string | null;
    messageStationId: string | null;
    lowerRatingForOnlineReception: number | null;

    constructor(params: ComplaintSubjectsListByIdsRatingForOnlineReceptionDTO) {
        this.subjectId = params.subjectId ?? null;
        this.messageStationId = params.messageStationId ?? null;
        this.lowerRatingForOnlineReception = params.lowerRatingForOnlineReception ?? null;
    }
}

export { ComplaintSubjectsListByIdsRatingForOnlineReception };
