import { SimpleObjectInterface } from '@thewhite/react-global-module/lib/interfaces';
import * as t from 'io-ts';
import ADDRESSABLE_TYPE from '../enums/addressableTypeEnum';
import BASE_STATUS from '../enums/baseStatusEnum';

export const ComplaintSubjectsListByIdsCategorySubjectType = t.interface({
    id: t.union([t.string, t.null]),
    name: t.union([t.string, t.null]),
    status: t.union([t.string, t.null]),
    description: t.union([t.string, t.null]),
    orderNumber: t.union([t.number, t.null]),
    addressableType: t.union([t.string, t.null]),
    hideNewComplaints: t.union([t.boolean, t.null]),
    extensibleOfResponseDueDate: t.union([t.boolean, t.null]),
});

export interface ComplaintSubjectsListByIdsCategorySubjectDTO extends t.TypeOf<typeof ComplaintSubjectsListByIdsCategorySubjectType> {
}

class ComplaintSubjectsListByIdsCategorySubject {
    id: string | null;
    name: string | null;
    description: string | null;
    orderNumber: number | null;
    hideNewComplaints: boolean | null;
    status: SimpleObjectInterface | null;
    extensibleOfResponseDueDate: boolean | null;
    addressableType: SimpleObjectInterface | null;

    constructor(params: ComplaintSubjectsListByIdsCategorySubjectDTO) {
        this.id = params.id ?? null;
        this.name = params.name ?? null;
        this.description = params.description ?? null;
        this.orderNumber = params.orderNumber ?? null;
        this.hideNewComplaints = params.hideNewComplaints ?? null;
        this.status = !!params.status ? BASE_STATUS[params.status] : null;
        this.extensibleOfResponseDueDate = params.extensibleOfResponseDueDate ?? null;
        this.addressableType = !!params.addressableType ? ADDRESSABLE_TYPE[params.addressableType] : null;
    }
}

export { ComplaintSubjectsListByIdsCategorySubject };
