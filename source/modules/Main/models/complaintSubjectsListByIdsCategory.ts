import { SimpleObjectInterface } from '@thewhite/react-global-module/lib/interfaces';
import * as t from 'io-ts';
import BASE_STATUS from '../enums/baseStatusEnum';
import { ComplaintSubjectsListByIdsCategorySubject, ComplaintSubjectsListByIdsCategorySubjectType } from './complaintSubjectsListByIdsCategorySubject';

export const ComplaintSubjectsListByIdsCategoryType = t.interface({
    id: t.union([t.string, t.null]),
    icon: t.union([t.string, t.null]),
    name: t.union([t.string, t.null]),
    status: t.union([t.string, t.null]),
    orderNumber: t.union([t.number, t.null]),
    subjects: t.array(ComplaintSubjectsListByIdsCategorySubjectType),
});

export interface ComplaintSubjectsListByIdsCategoryDTO extends t.TypeOf<typeof ComplaintSubjectsListByIdsCategoryType> {
}

class ComplaintSubjectsListByIdsCategory {
    id: string | null;
    icon: string | null;
    name: string | null;
    orderNumber: number | null;
    status: SimpleObjectInterface | null;
    subjects: ComplaintSubjectsListByIdsCategorySubject[]

    constructor(params: ComplaintSubjectsListByIdsCategoryDTO) {
        this.id = params.id ?? null;
        this.icon = params.icon ?? null;
        this.name = params.name ?? null;
        this.orderNumber = params.orderNumber ?? null;
        this.status = !!params.status ? BASE_STATUS[params.status] : null;
        this.subjects = !!params.subjects.length ? params.subjects.map(subject => new ComplaintSubjectsListByIdsCategorySubject(subject)) : [];
    }
}

export { ComplaintSubjectsListByIdsCategory };
