import * as t from 'io-ts';

export const ProcessQuestionsComplaintCloseType = t.interface({
    value: t.union([t.string, t.null]),
});

export interface IProcessQuestionsComplaintCloseDto extends t.TypeOf<typeof ProcessQuestionsComplaintCloseType> {
}

class ProcessQuestionsComplaintClose {
    value: string | null;

    constructor(params: IProcessQuestionsComplaintCloseDto) {
        this.value = params.value ?? null;
    }
}

export { ProcessQuestionsComplaintClose };
