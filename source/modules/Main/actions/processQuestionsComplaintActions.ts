import { WsReactBaseService } from '@thewhite/react-base-components';
import { BaseActions } from '@thewhite/react-global-module/lib/actions';
import { IProcessQuestionsComplaintCloseParams } from '../interfaces/IProcessQuestionsComplaintCloseParams';
import { IProcessQuestionsComplaintCloseDto, ProcessQuestionsComplaintClose, ProcessQuestionsComplaintCloseType } from '../models/processQuestionsComplaintClose';

export class ProcessQuestionsComplaintActions extends BaseActions {
    static DEFAULT_URL = '/api/complaint-service/process/questions/complaint/';

    /**
     * Создание тестового экшена
     */
    static close = (bodyParams: IProcessQuestionsComplaintCloseParams, complaintId: string): Promise<ProcessQuestionsComplaintClose | null> =>
        ProcessQuestionsComplaintActions.MODEL_API_RESOURCE<IProcessQuestionsComplaintCloseDto, ProcessQuestionsComplaintClose>({
            bodyParams,
            url: `${complaintId}/close`,
            method: WsReactBaseService.POST_METHOD,
            model: ProcessQuestionsComplaintClose,
            modelType: ProcessQuestionsComplaintCloseType,
        });
}
