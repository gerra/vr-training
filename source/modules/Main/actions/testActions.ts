import { WsReactBaseService } from '@thewhite/react-base-components';
import { BaseActions } from '@thewhite/react-global-module/lib/actions';
import { ITestCreateParams } from '../interfaces/ITestCreateParams';
import { TestCreate, TestCreateType } from '../models/testCreate';

export class TestActions extends BaseActions {
    static DEFAULT_URL = '/api/complaint-service/internal/complaint-subject-set/';

    /**
     * Создание тестового экшена
     */
    static create = (bodyParams: ITestCreateParams): Promise<TestCreate | null> =>
        TestActions.MODEL_API_RESOURCE({
            bodyParams,
            url: 'create',
            method: WsReactBaseService.POST_METHOD,
            model: TestCreate,
            modelType: TestCreateType,
        });
}
