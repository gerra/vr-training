import { WsReactBaseService } from '@thewhite/react-base-components';
import { BaseActions } from '@thewhite/react-global-module/lib/actions';
import { ICategorySubjectUpdate } from '../interfaces/ICategorySubjectUpdateParams';
import { IComplaintSubjectsListByIdsParams } from '../interfaces/IComplaintSubjectsListByIdsParams';
import { ComplaintSubjectsListByIds, ComplaintSubjectsListByIdsType, IComplaintSubjectListDTO } from '../models/complaintSubjectsListByIds';
import { ComplaintSubjectsListByIdsCategorySubject, ComplaintSubjectsListByIdsCategorySubjectDTO, ComplaintSubjectsListByIdsCategorySubjectType } from '../models/complaintSubjectsListByIdsCategorySubject';

export class ComplaintSubjectsListActions extends BaseActions {
    static DEFAULT_URL = '/api/complaint-subjects/list/';

    /**
     * Получить список тематик по списку идентификаторов
     */
    static listByIds = (queryParams: IComplaintSubjectsListByIdsParams): Promise<ComplaintSubjectsListByIds[]> =>
        ComplaintSubjectsListActions.ARRAY_API_RESOURCE<IComplaintSubjectListDTO, ComplaintSubjectsListByIds>({
            queryParams,
            url: 'by-ids',
            method: WsReactBaseService.GET_METHOD,
            model: ComplaintSubjectsListByIds,
            modelType: console.log(ComplaintSubjectsListByIdsType), // TODO: тупость какая-то
            testApi: true,
            testApiUrl: 'complaintSubjectsListByIds.json',
        });

    /**
     * Получить список тематик по списку идентификаторов
     */
    static update = (bodyParams: ICategorySubjectUpdate, id: string): Promise<ComplaintSubjectsListByIdsCategorySubject[]> =>
    ComplaintSubjectsListActions.ARRAY_API_RESOURCE<ComplaintSubjectsListByIdsCategorySubjectDTO, ComplaintSubjectsListByIdsCategorySubject>({
        bodyParams,
        url: `${id}/update`,
        method: WsReactBaseService.POST_METHOD,
        model: ComplaintSubjectsListByIdsCategorySubject,
        modelType: ComplaintSubjectsListByIdsCategorySubjectType,
    });
}
