import { SimpleObjectInterface } from '@thewhite/react-global-module/lib/interfaces';
import { TypeService } from '@thewhite/react-global-module/lib/services';

export enum natureTypeEnum {
    DEFAULT = 'DEFAULT',
    GRIEVANCE = 'GRIEVANCE',
}

export interface INatureType {
    [value: string]: SimpleObjectInterface;

    DEFAULT: SimpleObjectInterface;
    GRIEVANCE: SimpleObjectInterface;
}

/**
 * Типы адресов
 */
const NATURE_TYPE: INatureType = {
    DEFAULT: {
        name: 'Календарные дни',
        value: 'Дефолт',
    },
    GRIEVANCE: {
        name: 'Рабочие дни',
        value: 'Жалобы',
    },
};

export default NATURE_TYPE;

export const NatureTypeType = TypeService.createEnum<natureTypeEnum>(natureTypeEnum, 'natureTypeEnum');
