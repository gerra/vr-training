import { SimpleObjectInterface } from '@thewhite/react-global-module/lib/interfaces';
import { TypeService } from '@thewhite/react-global-module/lib/services';

export enum baseStatusEnum {
    ACTIVE = 'ACTIVE',
    DELETED = 'DELETED',
    NOT_ACTUAL = 'NOT_ACTUAL',
}

export interface IBaseStatus {
    [value: string]: SimpleObjectInterface;

    ACTIVE: SimpleObjectInterface;
    DELETED: SimpleObjectInterface;
    NOT_ACTUAL: SimpleObjectInterface;
}

/**
 * Типы адресов
 */
const BASE_STATUS: IBaseStatus = {
    ACTIVE: {
        name: 'Активно',
        value: 'ACTIVE',
    },
    DELETED: {
        name: 'Удалено',
        value: 'DELETED',
    },
    NOT_ACTUAL: {
        name: 'Неактивно',
        value: 'NOT_ACTUAL',
    },
};

export default BASE_STATUS;

export const BaseStatusType = TypeService.createEnum<baseStatusEnum>(baseStatusEnum, 'baseStatusEnum');
