import { SimpleObjectInterface } from '@thewhite/react-global-module/lib/interfaces';
import { TypeService } from '@thewhite/react-global-module/lib/services';

export enum decisionDaysTypeEnum {
    CALENDAR_DAYS = 'CALENDAR_DAYS',
    WORK_DAYS = 'WORK_DAYS',
}

export interface IDecisionDaysType {
    [value: string]: SimpleObjectInterface;

    CALENDAR_DAYS: SimpleObjectInterface;
    WORK_DAYS: SimpleObjectInterface;
}

/**
 * Типы адресов
 */
const DECISION_DAYS_TYPE: IDecisionDaysType = {
    CALENDAR_DAYS: {
        name: 'Календарные дни',
        value: 'CALENDAR_DAYS',
    },
    WORK_DAYS: {
        name: 'Рабочие дни',
        value: 'WORK_DAYS',
    },
};

export default DECISION_DAYS_TYPE;

export const DecisionDaysTypeType = TypeService.createEnum<decisionDaysTypeEnum>(decisionDaysTypeEnum, 'decisionDaysTypeEnum');
