import { SimpleObjectInterface } from '@thewhite/react-global-module/lib/interfaces';
import { TypeService } from '@thewhite/react-global-module/lib/services';

export enum AddressableTypeEnum {
    AUTO = 'AUTO',
    ANY_FROM_ROUTE = 'ANY_FROM_ROUTE',
    ANY = 'ANY',
}

export interface IAddressableType {
    [value: string]: SimpleObjectInterface;

    AUTO: SimpleObjectInterface;
    ANY_FROM_ROUTE: SimpleObjectInterface;
    ANY: SimpleObjectInterface;
}

/**
 * Типы адресов
 */
const ADDRESSABLE_TYPE: IAddressableType = {
    AUTO: {
        name: 'Автоматически',
        value: 'AUTO',
    },
    ANY_FROM_ROUTE: {
        name: 'Афиша',
        value: 'ANY_FROM_ROUTE',
    },
    ANY: {
        name: 'Любое',
        value: 'ANY',
    },
};

export default ADDRESSABLE_TYPE;

export const AddressableTypeType = TypeService.createEnum<AddressableTypeEnum>(AddressableTypeEnum, 'AddressableTypeEnum');
