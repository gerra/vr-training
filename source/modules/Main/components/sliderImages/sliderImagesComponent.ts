import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import { GlobalStore } from '../../../../store/globalStore';
import { SliderStore } from '../../../../store/sliderStore';
import { IImages } from '../../interfaces/IImages';
import sliderImagesTemplate from './sliderImagesTemplate';

export interface ISliderImagesProps {
    images: IImages[];
    globalStore?: GlobalStore;
    sliderStore: SliderStore;
}

export interface ISliderImagesState {
    images: IImages[];
    showSlider: number;
}

export interface ISliderImages extends WsReactBaseComponentInterface {
    intervalSlider: number;
    props: ISliderImagesProps;
    state: ISliderImagesState;

    clickDot(index?: number): void;
    slideLeft(index?: number): void;
    slideRight(index?: number): void;
}

@inject('globalStore', 'sliderStore')
@observer
class SliderImages extends WsReactBaseComponent<ISliderImagesProps, ISliderImagesState> implements ISliderImages {

    intervalSlider: number = 0;

    state: ISliderImagesState = {
        images: [],
        showSlider: 0,
    };

    componentDidMount() {
        this.upDateImages();
        this.intervalSlider = window.setInterval(() => this.slideRight(), 5000);
        this.props.sliderStore?.showNextSlider.observe(() => this.showNextSlider());
        window.addEventListener('resize', () => this.setState({ showSlider: this.props.sliderStore.index * -window.innerWidth }))
    };

    /**
     * Кладем поле images из props в state
     */
    upDateImages = () => {
        this.setState({ images: this.props.images });
    };

    /**
     * Показывает слдующий слайд
     */
    showNextSlider = () => {
        if (this.props.sliderStore?.getShowNextSlider()) return;
        setTimeout(() => this.props.sliderStore?.setShowNextSlider(true), 800);
    }

    /**
     * Переключает слайд вправо
     */
    slideRight = () => {
        if (!this.props.sliderStore?.getShowNextSlider()) return;
        this.updateInterval(true);
        if (this.props.sliderStore?.index !== this.state.images.length - 1) {
            this.props.sliderStore?.setSelectIndex(this.props.sliderStore?.index + 1);
        };
        if (this.state.showSlider === -window.innerWidth * (this.state.images.length - 1)) {
            this.props.sliderStore?.setSelectIndex(0);
            return this.setState({ showSlider: 0 });
        };
        this.setState({ showSlider: this.state.showSlider - window.innerWidth });
    };

    /**
     * Переключает слайд влево
     */
    slideLeft = () => {
        if (!this.props.sliderStore?.getShowNextSlider()) return;
        this.updateInterval(true);
        if (this.props.sliderStore?.index !== 0) {
            this.props.sliderStore?.setSelectIndex(this.props.sliderStore?.index - 1);
        };
        if (this.state.showSlider === 0) {
            this.props.sliderStore?.setSelectIndex(this.state.images.length - 1);
            return this.setState({ showSlider: -window.innerWidth * (this.state.images.length - 1) });
        };
        this.setState({ showSlider: this.state.showSlider + window.innerWidth });
    };

    /**
     * Что делать при нажатие на точку
     * @param index
     */
    clickDot = (index: number) => {
        if (!this.props.sliderStore?.getShowNextSlider()) return;
        this.updateInterval(true);
        this.setState({ showSlider: -window.innerWidth * index });
        this.props.sliderStore?.setSelectIndex(index);
    };

    /**
     * Обновление интервала при нажатие вправо, влево
     */
    updateInterval = (updateInterval?: boolean) => {
        if (updateInterval) {
            window.clearInterval(this.intervalSlider);
            this.intervalSlider = window.setInterval(() => this.slideRight(), 5000);
        };
        this.props.sliderStore?.setShowNextSlider(false);
    }

    render(): false | JSX.Element {
        return sliderImagesTemplate(this);
    }
}

export { SliderImages };
