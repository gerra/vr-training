import classnames from 'classnames';
import * as React from 'react';
import { ReactSVG } from 'react-svg';
import { FlexBox } from 'ws-react-flex-layout';
import { IImages } from '../../interfaces/IImages';
import SelectedSlider from '../selectedSlider/selectedSliderComponent';
import { ISliderImages } from './sliderImagesComponent';

const sliderImagesTemplate = (context: ISliderImages): JSX.Element => {
    return (
        <div>
            <FlexBox
                row={'ctr'}
                onClick={context.slideLeft}
                className={'slider-images__arrow-left'}
            >
                <ReactSVG
                    className={'slider-images__arrow-left-svg'}
                    src={'../assets/images/svg/arrow_left.svg'}
                />
            </FlexBox>
            <FlexBox
                row={'ctr'}
                onClick={context.slideRight}
                className={'slider-images__arrow-right'}
            >
                <ReactSVG
                    className={'slider-images__arrow-right-svg'}
                    src={'../assets/images/svg/arrow_right.svg'}
                />
            </FlexBox>
            <FlexBox
                row={'start'}
                className={'slider-images__images'}
                style={{ left: context.state.showSlider }}
            >
                {
                    context.state.images.map((image: IImages, index: number) =>
                        <img
                            key={index}
                            src={image.src}
                            className={classnames(
                                'slider-images__active-image',
                            )}
                        />,
                    )
                }
            </FlexBox>
            <FlexBox
                row={'ctr'}
                className={'slider-images__selected-slider'}
            >
                <SelectedSlider
                    clickDot={context.clickDot}
                    arrImages={context.props.images}
                />
            </FlexBox>
        </div>
    );
};

export default sliderImagesTemplate;
