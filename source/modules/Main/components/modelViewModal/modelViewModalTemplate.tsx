import React from 'react';
import { FlexBox } from 'ws-react-flex-layout';
import VRCheckBox from '../../../Global/components/VRCheckBox/VRCheckBoxComponent';
import VRInput from '../../../Global/components/VRInput/VRInputComponent';
import VRModal from '../../../Global/components/VRModal/VRModaComponent';
import VRSelect from '../../../Global/components/VRSelect/VRSelectComponent';
import ADDRESSABLE_TYPE from '../../enums/addressableTypeEnum';
import BASE_STATUS from '../../enums/baseStatusEnum';
import { IModelViewModal } from './modelViewModalComponent';

const modelViewModalTemplate = (context: IModelViewModal): JSX.Element => {
    return (
        <div>
            <VRModal
                title={'Игрок'}
                closeCallback={context.props.closeCallback}
                element={
                    <div>
                        <VRSelect
                            name={'Адресный тип'}
                            visibleField={'name'}
                            values={Object.values(ADDRESSABLE_TYPE)}
                            selectedValue={context.state.addressableType}
                            onChange={e => context.updateState(e?.target?.value ?? null, 'addressableType')}
                        />
                        <VRInput
                            label={'Описание'}
                            value={context.state.description ?? ''}
                            onChange={e => context.updateState(e?.target?.value ?? null, 'description')}
                        />
                        <VRCheckBox
                            name={'Расширяемая дата'}
                            value={context.state.extensibleOfResponseDueDate}
                            onClick={context.renewalExtensibleOfResponseDueDate}
                            onChange={e => context.updateState(e?.target?.value, 'extensibleOfResponseDueDate')}
                        />
                        <VRCheckBox
                            name={'Новые жалобы'}
                            value={context.state.hideNewComplaints}
                            onClick={context.renewalHideNewComplaints}
                            onChange={e => context.updateState(e?.target?.value, 'hideNewComplaints')}
                        />
                        <VRInput
                            label={'id'}
                            value={context.state.id ?? ''}
                            onChange={e => context.updateState(e?.target?.value ?? null, 'id')}
                        />
                        <VRInput
                            label={'Имя'}
                            value={context.state.name ?? ''}
                            onChange={e => context.updateState(e?.target?.value ?? null, 'name')}
                        />
                        <VRInput
                            label={'Номер'}
                            type={'number'}
                            value={context.state.orderNumber ?? ''}
                            onChange={e => context.updateState(e?.target?.value ?? null, 'orderNumber')}
                        />
                        <VRSelect
                            name={'Статус'}
                            visibleField={'name'}
                            values={Object.values(BASE_STATUS)}
                            selectedValue={context.state.status}
                            onChange={e => context.updateState(e?.target?.value ?? null, 'status')}
                        />
                        <FlexBox
                            row={'sb ctr'}
                            className={'model-view-modal'}
                        >
                            <button
                                className={'model-view-modal__button'}
                                onClick={() => context.props.closeCallback(false, true)}
                            >
                                info
                            </button>
                            <button
                                className={'model-view-modal__button'}
                                onClick={context.categorySubjectUpdate}
                            >
                                Сохранить
                            </button>
                        </FlexBox>
                    </div>
                }
            />
        </div>
    )
}

export default modelViewModalTemplate;
