import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import { GlobalStore } from '../../../../store/globalStore';
import { MagicModalStore } from '../../../../store/magicModalStore';
import { ComplaintSubjectsListActions } from '../../actions/complaintSubjectsListActions';
import { ICategorySubjectUpdate } from '../../interfaces/ICategorySubjectUpdateParams';
import { ComplaintSubjectsListByIdsCategorySubject } from '../../models/complaintSubjectsListByIdsCategorySubject';
import modelViewModalTemplate from './modelViewModalTemplate';

export interface IModelViewModalProps {
    globalStore?: GlobalStore;
    magicModalStore?: MagicModalStore;
    value: ComplaintSubjectsListByIdsCategorySubject;
    closeCallback: (value?: any, showInfoModal?: any) => any;
}

export interface IModelViewModalState {
    id: string | null;
    name: string | null;
    status: string | null;
    hideNewComplaints: boolean;
    description: string | null;
    orderNumber: number | null;
    addressableType: string | null;
    extensibleOfResponseDueDate: boolean;
}

export interface IModelViewModal extends WsReactBaseComponentInterface {
    props: IModelViewModalProps;
    state: IModelViewModalState;

    categorySubjectUpdate(): void;
    renewalHideNewComplaints(): void;
    renewalExtensibleOfResponseDueDate(): void;
}

@inject('globalStore', 'magicModalStore')
@observer
export default class ModelViewModal extends WsReactBaseComponent<IModelViewModalProps, IModelViewModalState> implements IModelViewModal {
    state: IModelViewModalState = {
        id: null,
        name: null,
        status: null,
        description: null,
        orderNumber: null,
        addressableType: null,
        hideNewComplaints: false,
        extensibleOfResponseDueDate: false,
    };

    componentDidMount() {
        this.addUpData()
    }

    /**
     * Складваем данные из props в state
     */
    addUpData = () => {
        this.setState({
            id: this.props.value.id,
            name: this.props.value.name,
            description: this.props.value.description,
            orderNumber: this.props.value.orderNumber,
            status: this.props.value.status?.name ?? null,
            hideNewComplaints: !!this.props.value.hideNewComplaints,
            addressableType: this.props.value.addressableType?.name ?? null,
            extensibleOfResponseDueDate: !!this.props.value.extensibleOfResponseDueDate,
        })
    }

    /**
     * Изменение флага extensibleOfResponseDueDate
     */
    renewalExtensibleOfResponseDueDate = () => {
        this.setState({ extensibleOfResponseDueDate: !this.state.extensibleOfResponseDueDate })
    }

    /**
     * Изменение флага hideNewComplaints
     */
    renewalHideNewComplaints = () => {
        this.setState({ hideNewComplaints: !this.state.hideNewComplaints })
    }

    /**
     * Дергаем хост subject
     */
    categorySubjectUpdate = () => {
        if (!this.state.id) return;

        let queryParams: ICategorySubjectUpdate = {
            id: this.state.id ?? null,
            name: this.state.name ?? null,
            status: this.state.status ?? null,
            orderNumber: this.state.orderNumber ?? null,
            description: this.state.description ?? null,
            addressableType: this.state.addressableType ?? null,
            hideNewComplaints: this.state.hideNewComplaints ?? null,
            extensibleOfResponseDueDate: this.state.extensibleOfResponseDueDate ?? null,
        };

        ComplaintSubjectsListActions.update(queryParams, 'Первый')
            .then(this.props.closeCallback(true))
    }

    render(): false | JSX.Element {
        return modelViewModalTemplate(this);
    }
}
