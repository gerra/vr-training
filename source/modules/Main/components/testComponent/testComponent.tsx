import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import { GlobalStore } from '../../../../store/globalStore';
import testComponentTemplate from './testComponentTemplate';

export interface ITestComponentProps {
    globalStore?: GlobalStore;
}

export interface ITestComponentState {
    value: string | null;
}

export interface ITestComponent extends WsReactBaseComponentInterface {
    redirect: any;
    reload: any;
    props: ITestComponentProps;
    state: ITestComponentState;
    // redirect: any;
    // reload: boolean;

    // goToState(stateName: string, params?: object): void;
}

@inject('globalStore')
@observer
class TestComponent extends WsReactBaseComponent<ITestComponentProps, ITestComponentState> implements ITestComponent {
    redirect: any;
    reload: any;
    state: ITestComponentState = {
        value: null,
    }

    render(): false | JSX.Element {
        return testComponentTemplate(this);
    }
}

export { TestComponent };
