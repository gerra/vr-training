import classnames from 'classnames';
import * as React from 'react';
import { Redirect } from 'react-router';
import { ITestComponent } from './testComponent';

const testComponentTemplate = (context: ITestComponent): JSX.Element => {
    if (context.reload) {
        context.reload = false;
        return <Redirect
            to={context.redirect}
            push={true}
        />;
    }

    return (
        <div
            className={classnames(
                'test-component',
            )}
        >
            <div
                className={classnames(
                    'test-component__button',
                )}
                onClick={() => context.goToState('/main-page')}
            >
                Перейти на другую страницу
            </div>
        </div>
    );
};

export default testComponentTemplate;
