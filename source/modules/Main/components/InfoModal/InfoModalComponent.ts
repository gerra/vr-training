import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import { MagicModalStore } from 'source/store/magicModalStore';
import { MainMenuStore } from 'source/store/mainMenuStore';
import { GlobalStore } from '../../../../store/globalStore';
import infoModalTemplate from './InfoModalTemplate';

export interface IInfoModalProps {
    globalStore?: GlobalStore;
    mainMenuStore?: MainMenuStore;
    magicModalStore?: MagicModalStore;
    closeInfoModal: (value: any) => any;
}

export interface IInfoModal extends WsReactBaseComponentInterface {
    props: IInfoModalProps;

    closeInfoModal(): void;
}

@inject('globalStore', 'mainMenuStore', 'magicModalStore')
@observer
export default class InfoModal extends WsReactBaseComponent<IInfoModalProps, {}> implements IInfoModal {
    /**
     * Закрытие модального окна
     */
    closeInfoModal = () => {
        this.props.magicModalStore?.closeMagicModal();
        this.props.mainMenuStore?.mouseMove()
    }

    render(): false | JSX.Element {
        return infoModalTemplate(this);
    }
}
