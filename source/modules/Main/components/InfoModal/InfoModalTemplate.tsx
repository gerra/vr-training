import React from 'react';
import { ReactSVG } from 'react-svg';
import { FlexBox } from 'ws-react-flex-layout';
import VRModal from '../../../Global/components/VRModal/VRModaComponent';
import { IInfoModal } from './InfoModalComponent';

const infoModalTemplate = (context: IInfoModal): JSX.Element => {
    return (
        <div
            className={'info-modal'}
        >
            {
                <VRModal
                    closeCallback={context.props.closeInfoModal}
                    title={'Приветсвуем!'}
                    element={
                        <div>
                            <div>
                                Чтоб узнать больше, напишите на почту: ayfPacanq@777.ru
                            </div>
                            <FlexBox
                                row={'ctr'}
                                className={'info-modal__sun'}
                            >
                                <ReactSVG
                                    src={'../assets/images/svg/sun.svg'}
                                />
                            </FlexBox>
                        </div>
                    }
                />
            }
        </div>
    )
}

export default infoModalTemplate;
