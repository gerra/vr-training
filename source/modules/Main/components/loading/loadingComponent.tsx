import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import classnames from 'classnames';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { FlexBox } from 'ws-react-flex-layout';
import { GlobalStore } from '../../../../store/globalStore';

export interface ILoadingProps {
    loading: boolean;
    globalStore?: GlobalStore;
}

export interface ILoading extends WsReactBaseComponentInterface {
    props: ILoadingProps;
}

@inject('globalStore')
@observer
export default class Loading extends WsReactBaseComponent<ILoadingProps, {}> implements ILoading {
    render(): false | JSX.Element {
        return (
            <FlexBox
                row={'ctr'}
                className={classnames(
                    this.props.loading ? 'loading' : 'loading__ending',
                )}
            >
                <img
                    src={'../assets/images/loading.gif'}
                    alt={'Загрузка'}
                />
            </FlexBox>
        );
    }
}
