import React from 'react';
import { FlexBox } from 'ws-react-flex-layout';
import Notification from '../../../Global/components/notification/notificationComponent';
import { ITableObject } from '../../interfaces/ITableObject';
import PlayerViewModal from '../playerViewModal/playerViewModalComponent';
import { IDisplayingList } from './displayingListComponent';

const displayingListTemplate = (context: IDisplayingList): JSX.Element => {
    return (
        <div
            className={'displaying-list'}
        >
            <FlexBox
                row={'center center'}
                className={'displaying-list__title'}
            >
                Игра
            </FlexBox>
            <FlexBox
                row={'center center'}
            >
                <table
                    className={'displaying-list__table'}
                >
                    <thead
                        className={'displaying-list__table-heading'}
                    >
                        <tr>
                            <th>#</th>
                            <th
                                onClick={() => context.outputStringInConsole('First player')}
                                className={'displaying-list__table-heading'}
                            >
                                First player
                            </th>
                            <th
                                onClick={() => context.outputStringInConsole('Second player')}
                                className={'displaying-list__table-heading'}
                            >
                                Second player
                            </th>
                            <th
                                onClick={() => context.outputStringInConsole('Result')}
                                className={'displaying-list__table-heading'}
                            >
                                Result
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            context.state.table.map((item: ITableObject, index: number) =>
                                <tr
                                    key={index}
                                    onClick={() => context.openPlayerViewModal(item)}
                                    className={'displaying-list__table-context-wrap'}
                                >
                                    <td
                                        className={'displaying-list__table-context'}
                                    >
                                        {item.number}
                                    </td>
                                    <td
                                        className={'displaying-list__table-context'}
                                    >
                                        {item.firstPlayer}
                                    </td>
                                    <td
                                        className={'displaying-list__table-context'}
                                    >
                                        {item.secondPlayer}
                                    </td>
                                    <td
                                        className={'displaying-list__table-context'}
                                    >
                                        {item.outcome}
                                    </td>
                                    <div
                                        className={'displaying-list__table-context-modal'}
                                    >
                                        <Notification
                                            notificationPlayers={item}
                                        />
                                    </div>
                                </tr>,
                            )
                        }
                    </tbody>
                </table>
            </FlexBox>
            {
                !!context.state.selectedItem &&
                <PlayerViewModal
                    value={context.state.selectedItem}
                    closeCallback={context.closePlayerViewModal}
                />
            }
        </div>
    )
}

export default displayingListTemplate;
