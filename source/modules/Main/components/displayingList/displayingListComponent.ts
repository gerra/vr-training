import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import { GlobalStore } from '../../../../store/globalStore';
import { MainMenuStore } from '../../../../store/mainMenuStore';
import { ITableObject } from '../../interfaces/ITableObject';
import displayingListTemplate from './displayingListTemplate';

export interface IDisplayingListProps {
    table: ITableObject[];
    globalStore?: GlobalStore;
    mainMenuStore?: MainMenuStore;
}

export interface IDisplayingListState {
    variable: boolean;
    table: ITableObject[];
    showBaseModal: boolean;
    alternant: string | null;
    selectedItem: ITableObject | null;
}

export interface IDisplayingList extends WsReactBaseComponentInterface {
    props: IDisplayingListProps;
    state: IDisplayingListState;

    closePlayerViewModal(): any;
    outputStringInConsole(nameTh: string): void;
    openPlayerViewModal(item: ITableObject): void;
}

@inject('globalStore', 'mainMenuStore')
@observer
export default class DisplayingList extends WsReactBaseComponent<IDisplayingListProps, IDisplayingListState> implements IDisplayingList {
    state: IDisplayingListState = {
        table: [],
        variable: true,
        alternant: null,
        selectedItem: null,
        showBaseModal: false,
    };

    componentDidMount = () => {
        this.tableFunction()
    }

    /**
     * Передача пропса
     */
    tableFunction = () => {
        if (!this.props.table) return;

        this.setState({
            table: this.props.table.filter(item => !item.hide),
        })
    }

    /**
     * Вывод строки в консоль
     */
    outputStringInConsole = (nameTh: string) => {
        console.log(nameTh);
    }

    /**
     * Открытие модального окна
     */
    openPlayerViewModal = (item: ITableObject) => {
        this.setState({ selectedItem: item });
        this.props.mainMenuStore?.stopMouseMove();
    }

    /**
     * Закрытие модального окна
     */
    closePlayerViewModal = (): any => {
        this.setState({ selectedItem: null });
        this.props.mainMenuStore?.mouseMove()
    }

    render(): false | JSX.Element {
        return displayingListTemplate(this);
    }
}
