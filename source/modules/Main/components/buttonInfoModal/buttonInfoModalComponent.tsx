import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { FlexBox } from 'ws-react-flex-layout';
import { GlobalStore } from '../../../../store/globalStore';
import { MagicModalStore } from '../../../../store/magicModalStore';
import { MainMenuStore } from '../../../../store/mainMenuStore';
import InfoModal from '../InfoModal/InfoModalComponent';

export interface IButtonInfoModalProps {
    globalStore?: GlobalStore;
    mainMenuStore?: MainMenuStore;
    magicModalStore?: MagicModalStore;
}

export interface IButtonInfoModalState {
    showInfoModal: boolean | null;
}

export interface IButtonInfoModal extends WsReactBaseComponentInterface {
    props: IButtonInfoModalProps;
    state: IButtonInfoModalState;
}

@inject('globalStore', 'mainMenuStore', 'magicModalStore')
@observer
export default class ButtonInfoModal extends WsReactBaseComponent<IButtonInfoModalProps, IButtonInfoModalState> implements IButtonInfoModal {
    state: IButtonInfoModalState = {
        showInfoModal: null,
    };

    componentDidMount() {
        this.props.magicModalStore?.showMagicModal.observe(() => this.forceUpdate());
    }

    /**
     * Открытие модального окна, прекращается слежка за мышью
     */
    openInfoModal = () => {
        this.props.magicModalStore?.openMagicModal()
        this.props.mainMenuStore?.stopMouseMove();
    };

    /**
     * Закрытие модального окна, слежка за мышью
     */
    closeInfoModal = () => {
        this.props.magicModalStore?.closeMagicModal()
        this.props.mainMenuStore?.mouseMove();
    };

    render(): false | JSX.Element {
        return (
            <div>
                {
                    !!this.props.magicModalStore?.getShowMagicModal() &&
                    <InfoModal
                        closeInfoModal={this.closeInfoModal}
                    />
                }
                <FlexBox
                    row={'end'}
                    className={'button-info-modal'}
                >
                    <button
                        className={'button-info-modal__wrapper'}
                        onClick={this.openInfoModal}
                    >
                        <FlexBox
                            row={'ctr'}
                        >
                            info
                        </FlexBox>
                    </button>
                </FlexBox>
            </div>
        )
    }
}
