import classnames from 'classnames';
import React from 'react';
import { FlexBox } from 'ws-react-flex-layout';
import { IMenuObject } from '../../interfaces/IMenuObject';
import { IMainTopMenu } from './mainTopMenuComponent';

const mainTopMenuTemplate = (context: IMainTopMenu): JSX.Element => {
    return (
        <div
            ref={node => (context.refMenu = node)}
            className={classnames(
                'main-top-menu',
                {
                    'main-top-menu__visible': context.props.mainMenuStore?.getShowMenu(),
                },
            )}
        >
            <FlexBox
                row={'start start'}
            >
                {
                    context.state.items.map((item: IMenuObject, index: number) =>
                        <div
                            key={index}
                            className={'main-top-menu__item-name'}
                        >
                            <FlexBox
                                row={'ctr'}
                            >
                                <a
                                    href={`#${item.href}`}
                                    onClick={() => context.goToState(item.href)}
                                    className={classnames(
                                        'main-top-menu__item',
                                        'ws-subheader-1-primary',
                                        {
                                            'main-top-menu__item-active': context.props.location.pathname === item.href,
                                        },
                                    )}
                                >
                                    {item.title}
                                </a>
                            </FlexBox>
                        </div>,
                    )
                }
            </FlexBox>
        </div>
    )
}

export default mainTopMenuTemplate;
