import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import { GlobalStore } from '../../../../store/globalStore';
import { MainMenuStore } from '../../../../store/mainMenuStore';
import { IMenuObject } from '../../interfaces/IMenuObject';
import mainTopMenuTemplate from './mainTopMenuTemplate';

export interface IMainTopMenuProps {
    location: any;
    globalStore?: GlobalStore;
    mainMenuStore?: MainMenuStore;
}

export interface IMainTopMenuState {
    items: IMenuObject[];
}

export interface IMainTopMenu extends WsReactBaseComponentInterface {
    props: IMainTopMenuProps;
    state: IMainTopMenuState;
    refMenu: HTMLDivElement | null;
}

@inject('globalStore', 'mainMenuStore')
@observer
export default class MainTopMenu extends WsReactBaseComponent<IMainTopMenuProps, IMainTopMenuState> implements IMainTopMenu {
    refMenu: HTMLDivElement | null = null;

    state: IMainTopMenuState = {
        items: [
            {
                title: 'Игроки',
                href: '/players-list',
            },
            {
                title: 'Карта',
                href: '/map',
            },
            {
                title: 'Арены',
                href: '/arenas',
            },
            {
                title: 'Слайдер',
                href: '/nature-slider',
            },
        ],
    };

    componentWillUmount() {
        this.props.mainMenuStore?.stopMouseMove();
    }

    componentDidMount() {
        this.props.mainMenuStore?.mouseMove();
        this.props.mainMenuStore?.setRefMenu(this.refMenu);
        this.props.mainMenuStore?.showMenu.observe(() => this.forceUpdate());
    }

    render(): false | JSX.Element {
        return mainTopMenuTemplate(this);
    }
}
