import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import classnames from 'classnames';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { FlexBox } from 'ws-react-flex-layout';
import { GlobalStore } from '../../../../store/globalStore';
import { SliderStore } from '../../../../store/sliderStore';
import { IImages } from '../../interfaces/IImages';

export interface ISelectedSliderProps {
    arrImages: IImages[];
    sliderStore?: SliderStore;
    globalStore?: GlobalStore;
    clickDot: (index: number) => void;
}

export interface ISelectedSlider extends WsReactBaseComponentInterface {
    props: ISelectedSliderProps;
}

@inject('globalStore', 'sliderStore')
@observer
export default class SelectedSlider extends WsReactBaseComponent<ISelectedSliderProps, {}> implements ISelectedSlider {
    render(): false | JSX.Element {
        return (
            <FlexBox
                row={'start'}
            >
                {this.props.arrImages.map((image, index: number) =>
                    <div
                        key={index}
                        onClick={() => this.props.clickDot(index)}
                        className={classnames(
                            'selected-slider__dot',
                            {
                                'selected-slider__select': this.props.sliderStore?.index === index,
                            },
                        )}
                    >
                    </div>,
                )}
                <h1>Hi</h1>
            </FlexBox>
        );
    }
}
