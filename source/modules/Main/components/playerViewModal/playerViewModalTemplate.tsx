import React from 'react';
import VRInput from '../../../Global/components/VRInput/VRInputComponent';
import VRModal from '../../../Global/components/VRModal/VRModaComponent';
import { IPlayerViewModal } from './playerViewModalComponent';

const playerViewModalTemplate = (context: IPlayerViewModal): JSX.Element => {
    return (
        <div>
            <VRModal
                title={`${context.state.number}`}
                closeCallback={context.props.closeCallback}
                element={
                    <div>
                        <VRInput
                            name={'First player'}
                            value={context.state.firstPlayer ?? ''}
                            onChange={e => context.updateState(e?.target?.value ?? null, 'firstPlayer')}
                        />
                        <VRInput
                            name={'Second player'}
                            value={context.state.secondPlayer ?? ''}
                            onChange={e => context.updateState(e?.target?.value ?? null, 'secondPlayer')}
                        />
                        <VRInput
                            disabled
                            name={'Result'}
                            value={context.state.outcome ?? ''}
                            onChange={e => context.updateState(e?.target?.value ?? null, 'outcome')}
                        />
                    </div>
                }
            />
        </div>
    )
}

export default playerViewModalTemplate;
