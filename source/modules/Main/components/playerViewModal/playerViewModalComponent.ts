import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import { GlobalStore } from '../../../../store/globalStore';
import { MainMenuStore } from '../../../../store/mainMenuStore';
import { ITableObject } from '../../interfaces/ITableObject';
import playerViewModalTemplate from './playerViewModalTemplate';

export interface IPlayerViewModalProps {
    value: ITableObject;
    globalStore?: GlobalStore;
    mainMenuStore?: MainMenuStore;
    closeCallback: (value?: any) => any;
}

export interface IPlayerViewModalState {
    number: number | null;
    outcome: string | null;
    firstPlayer: string | null;
    secondPlayer: string| null;
}

export interface IPlayerViewModal extends WsReactBaseComponentInterface {
    props: IPlayerViewModalProps;
    state: IPlayerViewModalState;
}

@inject('globalStore', 'mainMenuStore')
@observer
export default class PlayerViewModal extends WsReactBaseComponent<IPlayerViewModalProps, IPlayerViewModalState> implements IPlayerViewModal {
    state: IPlayerViewModalState = {
        number: null,
        outcome: null,
        firstPlayer: null,
        secondPlayer: null,
    };

    componentDidMount() {
        this.addUpData()
    }

    /**
     * Складваем данные из props в state
     */
    addUpData = () => {
        this.setState({
            number: this.props.value.number,
            outcome: this.props.value.outcome,
            firstPlayer: this.props.value.firstPlayer,
            secondPlayer: this.props.value.secondPlayer,
        })
    }

    render(): false | JSX.Element {
        return playerViewModalTemplate(this);
    }
}
