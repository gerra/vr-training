import { FlexBox } from '@thewhite/react-flex-layout';
import { WsReactInput } from '@thewhite/react-input';
import classnames from 'classnames';
import * as React from 'react';
import { IImageSlide, IMainPage } from './mainPageComponent';

const mainPageTemplate = (context: IMainPage): JSX.Element => {
    return (
        <div
            className={classnames(
                'main-page',
            )}
        >
            <div>
                <FlexBox
                    row="center center"
                    className={classnames(
                        'main-page__title',
                        'title-white',
                    )}
                >
                    <h1>SOLO.MA</h1>
                </FlexBox>
                <div
                    className={classnames(
                        'main-page__overlay',
                    )}
                />
                <section
                    className={classnames(
                        'main-page__header',
                    )}
                >
                    {
                        context.slidePool.map((slide: IImageSlide) => {
                            return (
                                <figure
                                    key={slide.slideId}
                                    className={classnames(
                                        'main-page__track-item', {
                                            'main-page__track-item--visible': context.state.viewportSlide?.slideId === slide.slideId,
                                        },
                                    )}
                                    style={{
                                        backgroundImage: `url('${slide.slideImagePath}')`,
                                    }}
                                />
                            );
                        })
                    }
                </section>
            </div>
            {/* <FlexBox
                row="start"
                className={classnames(
                    'main-page__header',
                )}
            >
                <div
                    className={classnames(
                        'main-page__header-left-wrap',
                    )}
                >
                    <FlexBox
                        row="end start"
                        className={classnames(
                            'main-page__header-left-title',
                        )}
                    >
                        <div
                            className={classnames(
                                'main-page__header-left-background',
                            )}
                        >
                        </div>
                        <div
                            className={classnames(
                                'main-page__header-left',
                            )}
                        >
                            <div
                                className={classnames(
                                    'main-page__title-name',
                                    'title-secondary',
                                )}
                            >
                                PHOTO
                            </div>
                        </div>
                    </FlexBox>
                </div>
                <FlexBox
                    row="end"
                    className={classnames(
                        'main-page__header-center',
                    )}
                >
                </FlexBox>
            </FlexBox> */}
            <FlexBox
                className={classnames(
                    'main-page__terms',
                )}
            >
                <FlexBox
                    row="center center"
                >
                    <div
                        className={classnames(
                            'main-page__term',
                        )}
                    >
                        <WsReactInput
                            topLabel
                            value={context.state.firstTerm}
                            type="number"
                            label="Первое слагаемое"
                            placeholder="Введите первое слагаемое"
                            updateCallback={e => context.updateState(e?.target?.value ?? '', 'firstTerm', () => context.additionActions())}
                        />
                    </div>
                    <WsReactInput
                        topLabel
                        value={context.state.secondTerm}
                        type="number"
                        label="Второе слагаемое"
                        placeholder="Введите второе слагаемое"
                        updateCallback={e => context.updateState(e?.target?.value ?? '', 'secondTerm', () => context.additionActions())}
                    />
                </FlexBox>
                <FlexBox
                    row="start"
                >
                    <div>
                        { context.state.message }
                    </div>
                    <div>
                        { context.state.amount }
                    </div>
                </FlexBox>
            </FlexBox>
        </div>
    );
};

export default mainPageTemplate;
