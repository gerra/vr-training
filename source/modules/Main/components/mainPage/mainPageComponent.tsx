import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { MainActions } from '../../actions/mainActions';
import { IMainParams } from '../../interfaces/IMain';
import { Main } from '../../models/additional';
import mainPageTemplate from './mainPageTemplate';

export interface IMainPageState {
    message: string | null;
    amount: number | null;
    answer: boolean;
    firstTerm: number;
    secondTerm: number;
    viewportSlide: IImageSlide;
}

export interface IImageSlide {
    slideId: string;
    index: number;
    slideImagePath: string;
}

export interface IMainPage extends WsReactBaseComponentInterface {
    state: IMainPageState;
    slidePool: IImageSlide[];
    defaultInterval: number;

    additionActions(): any;
}

class MainPageComponent extends WsReactBaseComponent<{}, IMainPageState> implements IMainPage {
    defaultInterval = 5000;
    slidePool = [
        {
            slideId: 'backgroundImageSoloma1',
            index: 1,
            slideImagePath: '../../../../assets/images/imageSlider/backgroundImageSoloma1.png',
        },
        {
            slideId: 'backgroundImageSoloma2',
            index: 2,
            slideImagePath: '../../../../assets/images/imageSlider/backgroundImageSoloma2.png',
        },
        {
            slideId: 'backgroundImageSoloma3',
            index: 3,
            slideImagePath: '../../../../assets/images/imageSlider/backgroundImageSoloma3.png',
        },
        {
            slideId: 'backgroundImageSoloma4',
            index: 4,
            slideImagePath: '../../../../assets/images/imageSlider/backgroundImageSoloma4.png',
        },
        {
            slideId: 'backgroundImageSoloma5',
            index: 5,
            slideImagePath: '../../../../assets/images/imageSlider/backgroundImageSoloma5.png',
        },
        {
            slideId: 'backgroundImageSoloma6',
            index: 6,
            slideImagePath: '../../../../assets/images/imageSlider/backgroundImageSoloma6.png',
        },
        {
            slideId: 'backgroundImageSoloma7',
            index: 7,
            slideImagePath: '../../../../assets/images/imageSlider/backgroundImageSoloma7.png',
        },
    ];

    state: IMainPageState = {
        message: null,
        amount: null,
        firstTerm: 0,
        answer: false,
        secondTerm: 0,
        viewportSlide: this.slidePool[0],
    }

    sliderInterval = window.setInterval(() => this.runSlider(), this.defaultInterval);

    componentDidMount() {
        window.onresize = () => {
            this.forceUpdate();
        }
    }

    componentWillUnmount() {
        window.clearInterval(this.sliderInterval);
    }

    /**
     * Запуск слайдера
     */
    runSlider = (): void => {
        const lastIndex: number = this.slidePool.length;
        const currentSlide: IImageSlide = this.state.viewportSlide;
        let nextSlide: IImageSlide;

        if (this.state.viewportSlide?.index < lastIndex) {
            nextSlide = this.slidePool[this.slidePool.findIndex((slide: IImageSlide) => slide.index === currentSlide.index + 1)]
        } else {
            nextSlide = this.slidePool[0];
        }

        this.setState({ viewportSlide: nextSlide });
    }

    /**
     * Сложение
     */
    additionActions = () => {
        let params: IMainParams = {
            a: this.state.firstTerm ?? 0,
            b: this.state.secondTerm ?? 0,
        }
        MainActions.test(params)
            .then((res: Main) => {
                this.setState({
                    message: res.message,
                    amount: res.amount,
                })
            })
    }

    render(): false | JSX.Element {
        return mainPageTemplate(this);
    }
}

export { MainPageComponent };
