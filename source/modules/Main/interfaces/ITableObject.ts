export interface ITableObject {
    number: number;
    firstPlayer: string;
    secondPlayer: string;
    outcome: string;
    hide: boolean;
}
