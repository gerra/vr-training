export interface IMenuObject {
    title: string;
    href: string;
}
