export interface ICategorySubjectUpdate {
    addressableType: string | null,
    description: string | null,
    extensibleOfResponseDueDate: boolean | null,
    hideNewComplaints: boolean | null,
    id: string | null,
    name: string | null,
    orderNumber: number | null,
    status: string | null
}
