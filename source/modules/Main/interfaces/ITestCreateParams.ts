export interface ITestCreateParams {
    description: string;
    name: string;
}
