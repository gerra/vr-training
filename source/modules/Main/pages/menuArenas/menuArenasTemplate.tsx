import * as React from 'react';
import { FlexBox } from 'ws-react-flex-layout';
import ButtonInfoModal from '../../components/buttonInfoModal/buttonInfoModalComponent';
import ModelViewModal from '../../components/modelViewModal/modelViewModalComponent';
import { ComplaintSubjectsListByIdsCategorySubject } from '../../models/complaintSubjectsListByIdsCategorySubject';
import { IMenuArenas } from './menuArenasComponent';

const menuArenasTemplate = (context: IMenuArenas): JSX.Element => {
    return (
        <div
            className={'menu-arenas'}
        >
            <FlexBox
                row={'center center'}
                className={'menu-arenas__title'}
            >
                Игроки мирового уровня:
            </FlexBox>
            <FlexBox
                row={'center center'}
                className={'menu-arenas__table-body'}
            >
                <table
                    className={'menu-arenas__table'}
                >
                    <thead
                        className={'menu-arenas__table-heading'}
                    >
                        <tr>
                            <th
                                className={'menu-arenas__table-heading'}
                            >
                                id
                            </th>
                            <th
                                className={'menu-arenas__table-heading'}
                            >
                                Игрок
                            </th>
                            <th
                                className={'menu-arenas__table-heading'}
                            >
                                Статус
                            </th>
                            <th
                                className={'menu-arenas__table-heading'}
                            >
                                Номер
                            </th>
                        </tr>
                    </thead>
                    <tbody
                        className={'menu-arenas__table-context'}
                    >
                        {
                            context.state.categorySubjects?.map((value: ComplaintSubjectsListByIdsCategorySubject, index: number) =>
                                <tr
                                    key={index}
                                    onClick={() => context.openModelViewModal(value)}
                                >
                                    <td
                                        className={'menu-arenas__table-context'}
                                    >
                                        {value?.id ?? ''}
                                    </td>
                                    <td
                                        className={'menu-arenas__table-context'}
                                    >
                                        {value?.name ?? ''}
                                    </td>
                                    <td
                                        className={'menu-arenas__table-context'}
                                    >
                                        {value?.status?.name ?? ''}
                                    </td>
                                    <td
                                        className={'menu-arenas__table-context'}
                                    >
                                        {value?.orderNumber ?? ''}
                                    </td>
                                </tr>,
                            )
                        }
                    </tbody>
                </table>
            </FlexBox>
            {
                !!context.state.selectedItem &&
                <ModelViewModal
                    value={context.state.selectedItem}
                    closeCallback={context.closeModelViewModal}
                />
            }
            <ButtonInfoModal/>
        </div>
    )
}

export default menuArenasTemplate;
