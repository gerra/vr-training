import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import { GlobalStore } from '../../../../store/globalStore';
import { MagicModalStore } from '../../../../store/magicModalStore';
import { MainMenuStore } from '../../../../store/mainMenuStore';
import { ComplaintSubjectsListActions } from '../../actions/complaintSubjectsListActions';
import { IComplaintSubjectsListByIdsParams } from '../../interfaces/IComplaintSubjectsListByIdsParams';
import { ComplaintSubjectsListByIds } from '../../models/complaintSubjectsListByIds';
import { ComplaintSubjectsListByIdsCategorySubject } from '../../models/complaintSubjectsListByIdsCategorySubject';
import menuArenasTemplate from './menuArenasTemplate';

export interface IMenuArenasProps {
    globalStore?: GlobalStore;
    mainMenuStore?: MainMenuStore;
    magicModalStore?: MagicModalStore;
}

export interface IMenuArenasState {
    uuid: string | null;
    selectedItem: ComplaintSubjectsListByIdsCategorySubject | null;
    categorySubjects: ComplaintSubjectsListByIdsCategorySubject[] | null;
}

export interface IMenuArenas extends WsReactBaseComponentInterface {
    props: IMenuArenasProps;
    state: IMenuArenasState;

    complaintSubjectsListByIds(): void;
    closeModelViewModal(update?: boolean): void;
    openModelViewModal(value: ComplaintSubjectsListByIdsCategorySubject): void;
}

@inject('globalStore', 'mainMenuStore', 'magicModalStore')
@observer
class MenuArenas extends WsReactBaseComponent<IMenuArenasProps, IMenuArenasState> implements IMenuArenas {

    state: IMenuArenasState = {
        uuid: 'uuid',
        selectedItem: null,
        categorySubjects: null,
    }

    componentDidMount = () => {
        this.complaintSubjectsListByIds();
    }

    /**
     * Дергаем хост для таблицы
     */
    complaintSubjectsListByIds = () => {
        if (!this.state.uuid) return;

        let queryParams: IComplaintSubjectsListByIdsParams = {
            ids: [this.state.uuid],
        };
        ComplaintSubjectsListActions.listByIds(queryParams)
            .then((res: ComplaintSubjectsListByIds[]) => {
                let categorySubjects: ComplaintSubjectsListByIdsCategorySubject[] = [];

                res.map((item: ComplaintSubjectsListByIds) => {
                    item.category?.subjects.map((subject: ComplaintSubjectsListByIdsCategorySubject) => {
                        categorySubjects.push(subject)
                    });
                });

                this.setState({ categorySubjects })
            });
    }

    /**
     * Открытие модального окна
     */
    openModelViewModal = (value: ComplaintSubjectsListByIdsCategorySubject) => {
        this.setState({ selectedItem: value });
        this.props.mainMenuStore?.stopMouseMove();
    }

    /**
     * Закрытие модального окна
     */
    closeModelViewModal = (update: boolean = false, showInfoModal: boolean = false): any => {
        this.setState(
            { selectedItem: null },
            () => {
                if (showInfoModal) {
                    this.props.magicModalStore?.openMagicModal();
                    this.props.mainMenuStore?.stopMouseMove();
                };
                if (update) this.complaintSubjectsListByIds();
            },
        );
        this.props.mainMenuStore?.mouseMove();
    }

    render(): false | JSX.Element {
        return menuArenasTemplate(this);
    }
}

export { MenuArenas };
