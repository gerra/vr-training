import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import MainTopMenu from '../../components/mainTopMenu/mainTopMenuComponent';
import { ITableObject } from '../../interfaces/ITableObject';
import playersListTemplate from './playersListTemplate';

export interface IPlayersListProps {
    table: MainTopMenu;
}

export interface IPlayersListState {
    spreadsheet: ITableObject[];
}

export interface IPlayersList extends WsReactBaseComponentInterface {
    props: IPlayersListProps;
    state: IPlayersListState;
}

export default class PlayersList extends WsReactBaseComponent<IPlayersListProps, IPlayersListState> implements IPlayersList {
    state: IPlayersListState = {
        spreadsheet: [
            {
                number: 1,
                firstPlayer: 'Pol',
                secondPlayer: 'Tiger',
                outcome: 'Pol',
                hide: false,
            },
            {
                number: 2,
                firstPlayer: 'Jacob',
                secondPlayer: 'Orlando',
                outcome: 'Orlando',
                hide: false,
            },
            {
                number: 3,
                firstPlayer: 'Fet',
                secondPlayer: 'Small',
                outcome: 'Small',
                hide: false,
            },
            {
                number: 4,
                firstPlayer: 'Gerra',
                secondPlayer: 'Boss',
                outcome: 'Boss',
                hide: false,
            },
        ],
    }

    render(): false | JSX.Element {
        return playersListTemplate(this)
    }
}
