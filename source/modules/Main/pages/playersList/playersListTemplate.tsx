import * as React from 'react';
import DisplayingList from '../../components/displayingList/displayingListComponent';
import { IPlayersList } from './playersListContainer';

const playersListTemplate = (context: IPlayersList): JSX.Element => {
    return (
        <div>
            <DisplayingList
                table={context.state.spreadsheet}
            />
        </div>
    )
}

export default playersListTemplate
