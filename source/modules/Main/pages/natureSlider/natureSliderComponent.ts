import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import { GlobalStore } from '../../../../store/globalStore';
import { IImages } from '../../interfaces/IImages';
import natureSliderTemplate from './natureSliderTemplate';

export interface INatureSliderProps {
    globalStore?: GlobalStore;
}

export interface INatureSliderState {
    images: IImages[];
}

export interface INatureSlider extends WsReactBaseComponentInterface {
    props: INatureSliderProps;
    state: INatureSliderState;
}

@inject('globalStore')
@observer
class NatureSlider extends WsReactBaseComponent<INatureSliderProps, INatureSliderState> implements INatureSlider {

    state: INatureSliderState = {
        images: [
            {
                src: '../assets/images/природа_1.jpg',
            },
            {
                src: '../assets/images/природа_2.jpg',
            },
            {
                src: '../assets/images/природа_3.jpg',
            },
            {
                src: '../assets/images/природа_4.jpg',
            },
        ],
    }

    render(): false | JSX.Element {
        return natureSliderTemplate(this);
    }
}

export { NatureSlider };
