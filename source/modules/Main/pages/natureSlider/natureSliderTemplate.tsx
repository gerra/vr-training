import * as React from 'react';
import { SliderImages } from '../../components/sliderImages/sliderImagesComponent';
import { INatureSlider } from './natureSliderComponent';

const natureSliderTemplate = (context: INatureSlider): JSX.Element => {
    return (
        <div>
            <SliderImages
                images={context.state.images}
            />
        </div>
    )
}

export default natureSliderTemplate;
