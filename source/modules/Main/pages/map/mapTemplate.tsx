import classnames from 'classnames';
import * as React from 'react';
import { Map, Placemark, Polygon, YMaps } from 'react-yandex-maps';
import { FlexBox } from 'ws-react-flex-layout';
import VRInput from '../../../Global/components/VRInput/VRInputComponent';
import Loading from '../../components/loading/loadingComponent';
import { IMap, IMapMarkers } from './mapComponent';

const mapTemplate = (context: IMap): JSX.Element => {
    return (
        <FlexBox
            className={'menu-map'}
            row={'sb start'}
        >
            <div
                className={'menu-map__left-block'}
            >
                <div
                    className={'menu-map__text'}
                >
                    <FlexBox
                        row={'ctr'}
                        className={classnames(
                            'menu-map__title',
                            'ws-page-header-secondary--bold',
                        )}
                    >
                        <h2>Яндекс.Карты</h2>
                    </FlexBox>
                    <span>
                        Яндекс.Карты — поисково-информационная картографическая служба Яндекса.
                        Открыта в 2004 году. Есть поиск по карте, информация о пробках, отслеживание городского транспорта,
                        прокладка маршрутов и панорамы улиц крупных и других городов[2]. Для России, Азербайджана, Андорры, Армении,
                        Белоруссии, Грузии, Израиля, Иордании, Ирака, Ирана, Казахстана, Кыргызстана, Латвии, Ливана, Литвы, Молдавии, Палестины, Польши, Сирии,
                        Таджикистана, Туркменистана, Узбекистана, Украины, Франции, Эстонии используются только собственные карты компании, которые обновляются раз в две недели;
                        данные для остальных государств и стран мира до 2017 года поставляла компания «НАВТЭК»[2], но затем компания перешла на
                        собственные данные[источник не указан 1231 день].
                    </span>
                </div>
                    <div
                        className={'menu-map__coords'}
                    >
                        <FlexBox
                            row={'sb ctr'}
                            className={'menu-map'}
                        >
                            <FlexBox
                                row={'ctr'}
                                className={'ws-page-header-secondary--bold'}
                            >
                                Координаты:
                            </FlexBox>
                            <div
                                className={'menu-map__color'}
                            >
                                <VRInput
                                    topLabel
                                    type={'color'}
                                    label={'Выбрать цвет, для метки:'}
                                    value={context.state.colorMarker ?? ''}
                                    onChange={e => context.updateState(e?.target?.value ?? null, 'colorMarker')}
                                />
                            </div>
                        </FlexBox>
                        <div
                            className={'menu-map__block-coords'}
                        >
                                {
                                    context.state.markers?.map((marker: IMapMarkers, index: number) =>
                                        <FlexBox
                                            key={index}
                                            row={'sb ctr'}
                                            className={'menu-map__coords'}
                                        >
                                            <span>
                                                Y:{marker.coordinates[0].toFixed(4)},  X:{marker.coordinates[1].toFixed(4)}
                                            </span>
                                            <button
                                                className={'menu-map__delete'}
                                                onClick={() => context.deleteMarker(index)}
                                            >
                                                Удалить
                                            </button>
                                        </FlexBox>,
                                    )
                                }
                        </div>
                    </div>
            </div>
            <FlexBox
                row={'ctr'}
                className={'menu-map__body'}
            >
                <YMaps>
                    <Map
                        className={'menu-map__map'}
                        onClick={context.changesMarkers}
                        defaultState={context.state.mapData}
                        onLoad={() => context.updateState(false, 'loading')}
                    >
                        {
                            context.state.markers?.map((marker: IMapMarkers, index: number) =>
                                <Placemark
                                    key={index}
                                    geometry={marker.coordinates}
                                    options={{ iconColor: marker.color }}
                                />,
                            )
                        }
                        <Polygon
                            geometry={[context.state.markers.map((marker: IMapMarkers) => marker.coordinates)]}
                        />
                    </Map>
                </YMaps>
            </FlexBox>
            <Loading
                loading={context.state.loading}
            />
        </FlexBox>
    )
}

export default mapTemplate;
