import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import { MapState, PlacemarkGeometry } from 'react-yandex-maps';
import { GlobalStore } from '../../../../store/globalStore';
import mapTemplate from './mapTemplate';

export interface IMapProps {
    globalStore?: GlobalStore;
}

export interface IMapMarkers {
    color: string;
    coordinates: PlacemarkGeometry;
}

export interface IMapState {
    loading: boolean;
    mapData: MapState;
    colorMarker: string;
    markers: IMapMarkers[];
}

export interface IMap extends WsReactBaseComponentInterface {
    props: IMapProps;
    state: IMapState;

    deleteMarker(index: number): void;
    changesMarkers(event?: any): void;
}

@inject('globalStore')
@observer
export class Map extends WsReactBaseComponent<IMapProps, IMapState> implements IMap {
    state: IMapState = {
        mapData: {
            zoom: 12,
            center: [48.4827, 135.0838],
        },
        markers: [],
        colorMarker: '#000000',
        loading: true,
    }

    /**
     * Меняет координаты и цвет метки
     */
    changesMarkers = (event?: any) => {
        let markers: IMapMarkers[] = this.state.markers;
        markers.push({
            color: this.state.colorMarker,
            coordinates: event.get('coords'),
        })
        this.setState({ markers });
    }

    /**
     * Удаление метки
     */
    deleteMarker = (index: number) => {
        let markers: IMapMarkers[] = this.state.markers;
        markers.splice(index, 1);
        this.setState({ markers });
    }

    render(): false | JSX.Element {
        return mapTemplate(this);
    }
}
