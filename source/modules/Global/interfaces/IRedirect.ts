export interface RedirectInterface {
    pathname: string;
    search: any;
}
