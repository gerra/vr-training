import { TypeService } from '@thewhite/react-global-module/lib/services';

export enum inputTypeEnum {
    COLOR = 'COLOR',
}

export interface IInputType {
    [value: string]: string;

    COLOR: string;
}

/**
 * Типы input
 */
const INPUT_TYPE: IInputType = {
    COLOR: 'color',
};

export default INPUT_TYPE;

export const BaseStatusType = TypeService.createEnum<inputTypeEnum>(inputTypeEnum, 'inputTypeEnum');
