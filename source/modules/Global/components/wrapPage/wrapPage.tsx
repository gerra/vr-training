import { WsReactBaseComponentInterface, WsReactBaseContainer } from '@thewhite/react-base-components';
import { observer } from 'mobx-react';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { GlobalStore } from '../../../../store/globalStore';
import WsVoiceInfinitySpinner from '../wsVoiceInfinitySpinner/wsVoiceInfinitySpinner';

interface WrapPageProps {
    globalStore?: GlobalStore;
    children: false | JSX.Element;
}

export interface WrapPageInterface extends WsReactBaseComponentInterface {
    props: WrapPageProps;
    startRedirect: boolean;
}

@observer
class WrapPage extends WsReactBaseContainer<WrapPageProps, {}> implements WrapPageInterface {
    startRedirect: boolean = false;

    render(): false | JSX.Element {
        if (!!this.props.globalStore && !!this.props.globalStore.redirect && !this.startRedirect) {
            this.startRedirect = true;
        } else if (!!this.startRedirect && !!this.props.globalStore) {
            this.startRedirect = false;
            this.props.globalStore.clearRedirect();
        }
        return (
            <>
                <WsVoiceInfinitySpinner
                    loading={!!this.props.globalStore ? this.props.globalStore.loading : false}
                    loadingText={!!this.props.globalStore ? this.props.globalStore.loadingText : null}
                    loadingShadowBackground={!!this.props.globalStore ? this.props.globalStore.loadingShadowBackground : false}
                />
                {
                    !!this.props.globalStore && !!this.props.globalStore.redirect && !!this.startRedirect
                        ?
                        <Redirect
                            to={this.props.globalStore.redirect}
                            push={true}
                        />
                        :
                        this.props.children
                }
            </>
        );
    }
}

export default WrapPage;
