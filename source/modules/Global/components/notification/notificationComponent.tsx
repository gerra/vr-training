import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import classnames from 'classnames';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { ITableObject } from 'source/modules/Main/interfaces/ITableObject';
import { FlexBox } from 'ws-react-flex-layout';
import { GlobalStore } from '../../../../store/globalStore';

export interface INotificationProps {
    globalStore?: GlobalStore;
    notificationPlayers: ITableObject;
}

export interface INotificationState {
    result: string | null;
    firstPlayer: string | null;
    secondPlayer: string | null;
}

export interface INotification extends WsReactBaseComponentInterface {
    props: INotificationProps;
    state: INotificationState;
}

@inject('globalStore')
@observer
export default class Notification extends WsReactBaseComponent<INotificationProps, INotificationState> implements INotification {
    state: INotificationState = {
        result: null,
        firstPlayer: null,
        secondPlayer: null,
    };

    componentDidMount = () => {
        this.renewsNotificationPlayers()
    }

    /**
     * Кладем значения из props в state
     */
    renewsNotificationPlayers = () => {
        this.setState({
            result: this.props.notificationPlayers.outcome,
            firstPlayer: this.props.notificationPlayers.firstPlayer,
            secondPlayer: this.props.notificationPlayers.secondPlayer,
        });
    }

    render(): false | JSX.Element {
        return (
            <div
                className={classnames(
                    'notification',
                    'ws-body-3-primary',
                )}
            >
                <div
                    className={'notification__context'}
                >
                    <FlexBox
                        row={'ctr'}
                        className={'notification__context-top'}
                    >
                            {this.state.firstPlayer} {this.state.secondPlayer}
                    </FlexBox>
                    <FlexBox
                        row={'ctr'}
                        className={'notification__context-down'}
                    >
                        {this.state.result}
                    </FlexBox>
                </div>
            </div>
        )
    }
}
