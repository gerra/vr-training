import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { GlobalStore } from '../../../../store/globalStore';

export interface IVRSelectProps {
    values: any;
    selectedValue: any;
    name: string | null;
    visibleField: string;
    globalStore?: GlobalStore;
    onChange?: (value: any) => any;
}

export interface IVRSelect extends WsReactBaseComponentInterface {
    props: IVRSelectProps;
}

@inject('globalStore')
@observer
export default class VRSelect extends WsReactBaseComponent<IVRSelectProps, {}> implements IVRSelect {
    render(): false | JSX.Element {
        return (
            <div
                className={'vr-select'}
            >
                <div
                    className={'vr-select__name'}
                >
                    {this.props.name}
                </div>
                <select
                    className={'vr-select__body'}
                    onChange={this.props.onChange}
                    value={!!this.props.selectedValue ? this.props.selectedValue[this.props.visibleField] : ''}
                >
                    {
                        this.props.values.map((item: any, index: number) => {
                            return (
                                <option
                                    key={index}
                                >
                                    {
                                        !!this.props.visibleField
                                            ? item[this.props.visibleField]
                                            : item
                                    }
                                </option>
                            )
                        })
                    }
                </select>
            </div>
        );
    }
}
