import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { ReactSVG } from 'react-svg';
import { FlexBox } from 'ws-react-flex-layout';
import { GlobalStore } from '../../../../store/globalStore';

export interface IVRCheckBoxProps {
    name: string;
    onClick?: any;
    value: boolean | null;
    globalStore?: GlobalStore;
    onChange?: (value: any) => any;
}

export interface IVRCheckBox extends WsReactBaseComponentInterface {
    props: IVRCheckBoxProps;
}

@inject('globalStore')
@observer
export default class VRCheckBox extends WsReactBaseComponent<IVRCheckBoxProps, {}> implements IVRCheckBox {
    render(): false | JSX.Element {
        return (
            <FlexBox
                row={'start'}
                className={'vr-checkbox'}
                onClick={this.props.onClick}
            >
                <div
                    className={'vr-checkbox__tick'}
                >
                    <ReactSVG
                        onChange={this.props.onChange}
                        src={this.props.value ? '../assets/images/svg/Checkbox.svg' : ''}
                    />
                </div>
                <div
                    className={'vr-checkbox__text'}
                >
                    {this.props.name}
                </div>
            </FlexBox>
        );
    }
}
