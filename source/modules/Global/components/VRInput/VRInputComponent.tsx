import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import classnames from 'classnames';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { GlobalStore } from '../../../../store/globalStore';
import INPUT_TYPE from '../../enums/inputTypeEnum';

export interface IVRInputProps {
    label: string;
    type?: string;
    disabled?: boolean;
    topLabel?: boolean;
    value: string | number;
    globalStore?: GlobalStore;
    onChange?: (value: any) => any;
}

export interface IVRInput extends WsReactBaseComponentInterface {
    props: IVRInputProps;
}

@inject('globalStore')
@observer
export default class VRInput extends WsReactBaseComponent<IVRInputProps, {}> implements IVRInput {
    render(): false | JSX.Element {
        return (
            <div
                className={classnames(
                    'vr-input',
                    {
                        'vr-input__row': this.props.topLabel,
                    },
                )}
            >
                <div
                    className={classnames(
                        'vr-input__label',
                        {
                            'vr-input__top-label': this.props.topLabel,
                        },
                    )}
                >
                    {this.props.label}
                </div>
                <div
                    className={classnames(
                        'vr-input__input',
                        {
                            'vr-input__input-color': this.props.type === INPUT_TYPE.COLOR,
                        },
                    )}
                >
                    <input
                        type={this.props.type}
                        value={this.props.value}
                        disabled={this.props.disabled}
                        onChange={this.props.onChange}
                    />
                </div>
            </div>
        );
    }
}
