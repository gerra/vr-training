import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { ReactSVG } from 'react-svg';
import { FlexBox } from 'ws-react-flex-layout';
import { GlobalStore } from '../../../../store/globalStore';

export interface IVRModalProps {
    title: string;
    element: JSX.Element;
    globalStore?: GlobalStore;
    closeCallback: (value?: any) => any;
}

export interface IVRModal extends WsReactBaseComponentInterface {
    props: IVRModalProps;
}

@inject('globalStore')
@observer
export default class VRModal extends WsReactBaseComponent<IVRModalProps, {}> implements IVRModal {
    render(): false | JSX.Element {
        return (
            <FlexBox
                row={'ctr'}
                className={'vr-modal__wrapper'}
                onClick={this.props.closeCallback}
            >
                <div
                    className={'vr-modal'}
                    onClick={e => e.stopPropagation()}
                >
                    <FlexBox
                        row={'sb ctr'}
                        className={'vr-modal__header'}
                    >
                        <h1>{this.props.title}</h1>
                        <button
                            className={'vr-modal__close'}
                            onClick={() => this.props.closeCallback()}
                        >
                            <ReactSVG
                                src={'../assets/images/svg/close_cross.svg'}
                            />
                        </button>
                    </FlexBox>
                    {this.props.element}
                </div>
            </FlexBox>
        )
    }
}
