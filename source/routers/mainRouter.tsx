import { Provider } from 'mobx-react';
import * as React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import NotFound from '../modules/Global/components/notFound/notFoundComponent';
import { MainPageComponent } from '../modules/Main/components/mainPage/mainPageComponent';
import { TestComponent } from '../modules/Main/components/testComponent/testComponent';
import { Map } from '../modules/Main/pages/map/mapComponent';
import { MenuArenas } from '../modules/Main/pages/menuArenas/menuArenasComponent';
import { NatureSlider } from '../modules/Main/pages/natureSlider/natureSliderComponent';
import PlayersList from '../modules/Main/pages/playersList/playersListContainer';
import MainWrap from '../modules/Wrappers/mainWrap/mainWrapComponent';
import globalStore from '../store/globalStore';
import magicModalStore from '../store/magicModalStore';
import mainMenuStore from '../store/mainMenuStore';
import sliderStore from '../store/sliderStore';

const stores = { globalStore, mainMenuStore, magicModalStore, sliderStore };

export default class MainRouter extends React.Component<{
    history?: any;
    location?: any;
}> {
    render() {
        return (
            <Provider
                {...stores}
            >
                <HashRouter>
                    <Switch>
                        <Route
                            exact
                            path="/"
                            component={TestComponent}
                        />
                        <MainWrap
                            location={this.props.location}
                        >
                                <Route
                                    path="/main-page"
                                    component={MainPageComponent}
                                />
                                <Route
                                    path="/players-list"
                                    component={PlayersList}
                                />
                                <Route
                                    path="/map"
                                    component={Map}
                                />
                                <Route
                                    path="/arenas"
                                    component={MenuArenas}
                                />
                                <Route
                                    path="/nature-slider"
                                    component={NatureSlider}
                                />
                        </MainWrap>
                        <Route
                            component={NotFound}
                        />
                    </Switch>
                </HashRouter>
            </Provider>
        );
    }
}
