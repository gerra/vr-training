import { action, IObservableValue, observable } from 'mobx';

class SliderStore {
    @observable index: number;
    @observable showNextSlider: IObservableValue<boolean>;

    constructor() {
        this.index = 0;
        this.showNextSlider = observable.box<boolean>(true);
    };

    @action('set select index')
    setSelectIndex = (index: number) => {
        this.index = index;
    };

    @action('get show next slider')
    getShowNextSlider = (): boolean => {
        return this.showNextSlider.get();
    };

    @action('set show next slider')
    setShowNextSlider = (showNextSlider: boolean) => {
        this.showNextSlider.set(showNextSlider);
    };
}

const sliderStore = new SliderStore();

export default sliderStore;
export { SliderStore };
