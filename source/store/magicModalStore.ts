import { action, IObservableValue, observable } from 'mobx';

class MagicModalStore {
    @observable showMagicModal: IObservableValue<boolean>;

    constructor() {
        this.showMagicModal = observable.box<boolean>(false);
    }

    /**
     * Открыть модальное окно
     */
    @action('open magic modal')
    openMagicModal = () => {
        this.showMagicModal.set(true);
    };

    /**
     * Закрыть модальное окно
     */
    @action('close magic modal')
    closeMagicModal = () => {
        this.showMagicModal.set(false);
    };

    @action('get show magic modal')
    getShowMagicModal = (): boolean => {
        return this.showMagicModal.get();
    };
}

const magicModalStore = new MagicModalStore();

export default magicModalStore;
export { MagicModalStore };
