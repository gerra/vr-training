import { action, IObservableValue, observable } from 'mobx';

class MainMenuStore {
    @observable showMenuHeight: number;
    @observable refMenu: HTMLDivElement | null;
    @observable showMenu: IObservableValue<boolean>;

    constructor() {
        this.refMenu = null;
        this.showMenuHeight = 20;
        this.showMenu = observable.box<boolean>(false);
    }

    /**
     * Отслеживаание мыши
     */
    mouseMove = () => {
        window.addEventListener('mousemove', this.trackMouse);
    }

    /**
     * Отмена отслеживания мыши
     */
    stopMouseMove = () => {
        window.removeEventListener('mousemove', this.trackMouse);
    }

    @action('track mouse')
    trackMouse = (event: MouseEvent) => {
        if (!this.refMenu) return;

        if (event.pageY < this.showMenuHeight) this.openMenu();
        if (this.refMenu.clientHeight < event.pageY) this.closeMenu();
    }

    @action('get show menu')
    getShowMenu = (): boolean => {
        return this.showMenu.get();
    };

    /**
     * Открыть меню
     */
    @action('open menu')
    openMenu = () => {
        if (this.getShowMenu()) return;
        this.showMenu.set(true);
    };

    /**
     * Закрыть меню
     */
    @action('close menu')
    closeMenu = () => {
        if (!this.getShowMenu()) return;
        this.showMenu.set(false);
    };

    /**
     * Изменение высоты показа меню
     */
    @action('change show menu height')
    changeShowMenuHeight = (showMenuHeight: number) => {
        this.showMenuHeight = showMenuHeight;
    };

    @action('set ref menu')
    setRefMenu = (refMenu: HTMLDivElement | null) => {
        this.refMenu = refMenu;
    };
}

const mainMenuStore = new MainMenuStore();

export default mainMenuStore;
export { MainMenuStore };
