import gulp from 'gulp';
import config from './gulp/config.json';
import { indexHtml } from './gulp/tasks';
import { assetsFiles } from './gulp/tasks/assetsFiles';
import { buildServe } from './gulp/tasks/build';
import { clean } from './gulp/tasks/clean';
import { js } from './gulp/tasks/js';
import { sass, extStyle } from './gulp/tasks/style';
import { watch } from './gulp/tasks/watch';
import { browserSyncInit } from './gulp/utils/browserSync';
import { php } from './gulp/tasks/php';

gulp.task('serve',
    gulp.series(
        buildServe(),
        gulp.parallel(watch, (callback) => {
            browserSyncInit(`${config.DIST_PATH}/`, callback);
        }),
    ));

gulp.task('build',
    gulp.series(
        clean,
        gulp.parallel(indexHtml, php, assetsFiles, extStyle, sass),
        js(),
    ));

gulp.task('default', gulp.series('serve'));
