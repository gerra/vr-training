import gulp from 'gulp';
import config from '../config';

function php() {
    return gulp.src(config.php.src)
        .pipe(gulp.dest(config.php.dst));
}

export { php };
