import gulp from 'gulp';
import { assetsFiles } from './assetsFiles';
import { clean } from './clean';
import { indexHtml } from './index';
import { sass, extStyleServe } from './style';
import { jsLint } from './js';
import { tsLint } from './ts';
import { php } from './php';

function buildServe() {
    return gulp.series(
        clean,
        gulp.parallel(tsLint, jsLint, php, indexHtml, assetsFiles, extStyleServe, sass),
    );
}

export { buildServe };
