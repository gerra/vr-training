import webpack from 'webpack';
import { webpackSettings } from './webpack';

const requireDevDependency = (moduleName) => {
    return process.env.NODE_ENV !== 'production'
        ? require(moduleName)
        : null;
};

const browserSync = requireDevDependency('browser-sync');
const proxyMiddleware = requireDevDependency('http-proxy-middleware');
const webpackDevMiddleware = requireDevDependency('webpack-dev-middleware');
const webpackHotMiddleware = requireDevDependency('webpack-hot-middleware');

const webpackDevServerConfig = {
    publicPath: '/',
    quiet: false,
    noInfo: false,
    logLevel: 'error',
};

const browserSyncInit = (baseDir, callback = null) => {
    if (!browserSync || !webpackDevMiddleware || !webpackHotMiddleware) {
        return;
    }

    let server = {
        baseDir: baseDir,
    };
    const target = 'http://localhost';

    const bundler = webpack(webpackSettings());

    server.middleware = [
        webpackDevMiddleware(bundler, webpackDevServerConfig),
        webpackHotMiddleware(bundler),
        proxyMiddleware(['/api'], {
            target: target,
            onError: onProxyError,
            ws: true,
        }),
    ];

    browserSync.instance = browserSync.init({
        startPath: '/#/players-list',
        server: server,
        browser: 'default',
    });

    // баг на macOs, корректный ответ от прокси интерпритируется как ошибочный,
    // поэтому мы сами обрабатываем ошибку
    function onProxyError(err, req, res) {
        let host = (req.headers && req.headers.host);
        console.error(`Error occured while trying to proxy to: ${host}${req.url}/n${err}`);
        res.end();
    }

    if (callback) {
        callback();
    }
};

export { browserSyncInit };
